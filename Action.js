//Get DOM elements from html file
//Loan Elements
const balanceElement = document.getElementById("balance");
const outstandingElement = document.getElementById("outstanding");
const loanElement = document.getElementById("loanAmount");
const labelElement = document.getElementById("outstandingLabel");

//Work Elements
const payElement = document.getElementById("pay");
const repayElement = document.getElementById("repay");

//Laptop Elements
const laptopsElement = document.getElementById("laptops");
const specsElement = document.getElementById("specs");
const titleElement = document.getElementById("title");
const descriptionElement = document.getElementById("description");
const priceElement = document.getElementById("price");
const imgElement = document.getElementById("img");

let laptops = [];
let transferAmount = 0;
let payLoan = 0;
let amount = 0;

//Checks if there is an outstanding loan to show or not
const showLoan = () => {
    if (outstandingElement.value > 0) {
        outstandingElement.type = "text";
        loanElement.style.visibility = "hidden";
        labelElement.style.visibility = "visible";
        repayElement.style.visibility = "visible";
    }
    else {
        outstandingElement.type = "hidden";
        loanElement.style.visibility = "visible";
        labelElement.style.visibility = "hidden";
        repayElement.style.visibility = "hidden";
    }
}

//Requests user input for loan
//Check if wanted loan is more than 2x of balance and oustanding loan is 0
const checkLoanAmount = () => {
    amount = prompt("Please enter the amount you want to loan");
    if(amount != 0) {
        if(amount > (balanceElement.value * 2)) {
            const warning = alert("Loan not accepted, loan amount too great");
        } else {
            outstandingElement.value = amount;
            balanceElement.value = parseInt(balanceElement.value) + parseInt(amount);
        }
        showLoan();
    }
}

//Increase pay amount when work button is clicked
const increasePay = () => {
    payElement.value = parseInt(payElement.value) + 100;
}

//Transfer money from pay balance to bank balance and pay loan
const transferMoney = () => {
    transferAmount = parseInt(payElement.value);
    if(outstandingElement.value > 0) {
        payLoan = transferAmount * 0.1;
        if(payLoan > outstandingElement.value) {
            payLoan = outstandingElement.value;
        }
        transferAmount = transferAmount - payLoan
        outstandingElement.value = parseInt(outstandingElement.value) - payLoan
    }
    balanceElement.value = parseInt(balanceElement.value) + transferAmount;
    payElement.value = 0;
    showLoan();
}

const repayLoan = () => {
    transferAmount = parseInt(payElement.value);
    if(transferAmount > outstandingElement.value) {
        transferAmount = outstandingElement.value;
    }
        outstandingElement.value = parseInt(outstandingElement.value) - transferAmount,
        payElement.value = parseInt(payElement.value) - transferAmount;
        showLoan();
}

//fetch laptops with price from API and insert into the select tag
fetch("https://noroff-komputer-store-api.herokuapp.com/computers")
    .then(response => response.json())
    .then(data => laptops = data)
    .then(laptops => addLaptopsToMenu(laptops));

const addLaptopsToMenu = (laptops) => {
    laptops.forEach(x => addLaptopToMenu(x));
    priceElement.innerText = laptops[0].price.toFixed(2);
    imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + laptops[0].image;
    titleElement.innerText = laptops[0].title;
    descriptionElement.innerText = laptops[0].description;
    specsElement.innerText = laptops[0].specs.toString().split(",").join("\n");
}

const addLaptopToMenu = (laptop) => {
    const laptopElement = document.createElement("option");
    laptopElement.value = laptop.id;
    laptopElement.appendChild(document.createTextNode(laptop.title));
    laptopsElement.appendChild(laptopElement);
}

const handleLaptopMenuChange = e => {
    const selectedLaptop = laptops[e.target.selectedIndex];
    priceElement.innerText = selectedLaptop.price.toFixed(2);
    //The id5 url is actually a png, but is listed as a jpg in the noroff url, this if-test fixes that
    if(selectedLaptop.id===5){
        imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/assets/images/5.png";
    } else {
        imgElement.src = "https://noroff-komputer-store-api.herokuapp.com/" + selectedLaptop.image;
    }
    titleElement.innerText = selectedLaptop.title;
    descriptionElement.innerText = selectedLaptop.description;
    specsElement.innerText = selectedLaptop.specs.toString().split(",").join("\n");
}

//
const purchaseLaptop = (laptops) => {
    if(parseInt(priceElement.innerText) > balanceElement.value) {
        const no = alert("Unable to purchase, please check funds");
    }
    else {
        balanceElement.value = parseInt(balanceElement.value) - parseInt(priceElement.innerText);
        const purchased = alert("Successfully purchased laptop, enjoy!")
    }
}


//Event listeners
laptopsElement.addEventListener("change", handleLaptopMenuChange);